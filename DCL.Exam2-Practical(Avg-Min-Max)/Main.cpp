
// Exam 2 (Avg, Min, Max)
// Devon Lozier

#include <iostream>
#include <conio.h>
#include <fstream>

using namespace std;

//function protocol
double FindAverage(double numInput[5]);
double FindMax(double numInput[5]);
double FindMin(double numInput[5]);
void PrintNumbers(double numbers[5]);
void SaveNumbers(double numbers[5], ostream &os);

int main()
{
	const int SIZE = 5;
	double userInput[SIZE];
	double total = 0;

	cout << "Enter five numbers to calculate Average, Max, and Min:\n";
	
	for (int i = 0; i < SIZE; i++)
	{
		cout << "Enter number " << i+1 << ": ";
		cin >> userInput[i];
	}

	PrintNumbers(userInput);

	string input;

	cout << "\n\nWould you like to save the numbers to a text file?\n\n" 
		    "Enter Y to save or anything else to close" << "\n";

	cin >> input;

	if (input == "Y" || input == "y")
	{
		string filepath = "Numbers-Calculations.txt";
		ofstream ofs;
		ofs.open(filepath);
		if (ofs)
		{
			SaveNumbers(userInput, ofs);
			cout << "Saved! ";
		}
		ofs.close();
		cout << "Press anything to close.";
	}


	(void)_getch();
	return 0;
}

double FindMax(double numInput[5])
{
	if (numInput[0] >= numInput[1] && numInput[0] >= numInput[2]
		&& numInput[0] >= numInput[3] && numInput[0] >= numInput[4])
	{
		return numInput[0];
	}
	else if (numInput[1] >= numInput[2] && numInput[1] >= numInput[3]
		&& numInput[1] >= numInput[4])
	{
		return numInput[1];
	}
	else if (numInput[2] >= numInput[3] && numInput[2] >= numInput[4])
	{
		return numInput[2];
	}
	else if (numInput[3] >= numInput[4])
	{
		return numInput[3];
	}
	return numInput[4];
}

double FindMin(double numInput[5])
{
	if (numInput[0] <= numInput[1] && numInput[0] <= numInput[2]
		&& numInput[0] <= numInput[3] && numInput[0] <= numInput[4])
	{
		return numInput[0];
	}
	else if (numInput[1] <= numInput[2] && numInput[1] <= numInput[3]
		&& numInput[1] <= numInput[4])
	{
		return numInput[1];
	}
	else if (numInput[2] <= numInput[3] && numInput[2] <= numInput[4])
	{
		return numInput[2];
	}
	else if (numInput[3] <= numInput[4])
	{
		return numInput[3];
	}
	return numInput[4];
}

double FindAverage(double numInput[5])
{
	double total = 0;

	for (int i = 0; i < 5; i++)
	{
		total += numInput[i];
	}
	return total / 5;
}

void PrintNumbers(double numbers[5])
{
	cout << "Your numbers: ";

	for (int i = 0; i < 5; i++)
	{
		cout << "\n" << numbers[i];
	}
	cout << "\nAverage: " << FindAverage(numbers);
	cout << "\nMaximum: " << FindMax(numbers);
	cout << "\nMinimum: " << FindMin(numbers);
}

void SaveNumbers(double numbers[5], ostream &os)
{
	os << "Your numbers: ";

	for (int i = 0; i < 5; i++)
	{
		os << "\n" << numbers[i];
	}
	os << "\nAverage: " << FindAverage(numbers);
	os << "\nMinimum: " << FindMin(numbers);
	os << "\nMaximum: " << FindMax(numbers);
}